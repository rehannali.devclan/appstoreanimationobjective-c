//
//  Enumerations.h
//  TestAnimation
//
//  Created by Developers Clan on 13/11/2018.
//  Copyright © 2018 Developers Clan. All rights reserved.
//

#ifndef Enumerations_h
#define Enumerations_h

typedef NS_ENUM(NSUInteger, CardVerticalExpandingStyle) {
	CardVerticalExpandingStyleTop,
	CardVerticalExpandingStyleCenter
};

#endif /* Enumerations_h */
