//
//  Constants.m
//  TestAnimation
//
//  Created by Developers Clan on 13/11/2018.
//  Copyright © 2018 Developers Clan. All rights reserved.
//

#import "Constants.h"

@implementation Constants
BOOL isEnabledDebugAnimatingViews = NO;
BOOL isEnabledAllowsUserInteractionWhileHighlightingCard = YES;
BOOL isEnabledWeirdTopInsetsFix = YES;
CardVerticalExpandingStyle cardVerticalExpandingStyle = CardVerticalExpandingStyleTop;
CGFloat cardCornerRadius = 16;
CGFloat cardHighlightedFactor = 0.96;
CGFloat dismissalAnimationDuration = 0.6;
@end
