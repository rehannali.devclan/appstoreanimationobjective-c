//
//  UIView+Anchor.m
//  TestAnimation
//
//  Created by Developers Clan on 12/11/2018.
//  Copyright © 2018 Developers Clan. All rights reserved.
//

#import "UIView+Anchor.h"

@implementation UIView (Anchor)
- (void)edges:(UIView *)toView top:(CGFloat)top left:(CGFloat)left right:(CGFloat)right bottom:(CGFloat)bottom {
	self.translatesAutoresizingMaskIntoConstraints = NO;
	NSArray<NSLayoutConstraint *> *constaints = @[
												  [self.topAnchor constraintEqualToAnchor:toView.topAnchor constant:top],
												  [self.bottomAnchor constraintEqualToAnchor:toView.bottomAnchor constant:-bottom],
												  [self.leadingAnchor constraintEqualToAnchor:toView.leadingAnchor constant:left],
												  [self.trailingAnchor constraintEqualToAnchor:toView.trailingAnchor constant:-right]
												  ];
	[NSLayoutConstraint activateConstraints:constaints];
}
@end
