//
//  DismissCardAnimator.h
//  TestAnimation
//
//  Created by Developers Clan on 13/11/2018.
//  Copyright © 2018 Developers Clan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DismissCardAnimator : NSObject <UIViewControllerAnimatedTransitioning>
- (instancetype)init:(struct Params)params;
@end

NS_ASSUME_NONNULL_END
