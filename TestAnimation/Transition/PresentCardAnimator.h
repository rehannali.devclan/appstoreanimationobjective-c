//
//  PresentCardAnimator.h
//  TestAnimation
//
//  Created by Developers Clan on 12/11/2018.
//  Copyright © 2018 Developers Clan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

struct ParamsPresent {
	CGRect fromCardFrame;
	CardTableCell *fromCell;
};

@interface PresentCardAnimator : NSObject <UIViewControllerAnimatedTransitioning>

- (instancetype)init:(struct ParamsPresent)params;

@end

NS_ASSUME_NONNULL_END
