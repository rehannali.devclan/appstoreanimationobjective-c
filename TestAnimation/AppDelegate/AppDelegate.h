//
//  AppDelegate.h
//  TestAnimation
//
//  Created by Developers Clan on 09/11/2018.
//  Copyright © 2018 Developers Clan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

